
POP_cat_multicolor.png
size: 3428,3428
format: RGBA8888
filter: Linear,Linear
repeat: none
12
  rotate: false
  xy: 906, 75
  size: 117, 61
  orig: 117, 70
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 1908, 26
  size: 109, 66
  orig: 109, 67
  offset: 0, 0
  index: -1
14
  rotate: false
  xy: 2840, 108
  size: 118, 53
  orig: 118, 54
  offset: 0, 0
  index: -1
Attack
  rotate: false
  xy: 2436, 83
  size: 86, 110
  orig: 89, 113
  offset: 1, 2
  index: -1
Blue/Gun
  rotate: false
  xy: 2365, 1322
  size: 326, 224
  orig: 326, 224
  offset: 0, 0
  index: -1
Blue/L_eye_catbat
  rotate: false
  xy: 1370, 1615
  size: 24, 44
  orig: 26, 46
  offset: 1, 1
  index: -1
Blue/L_hand_cataxe
  rotate: false
  xy: 1892, 1580
  size: 118, 198
  orig: 120, 198
  offset: 1, 0
  index: -1
Blue/L_hand_catbat
  rotate: false
  xy: 3162, 440
  size: 96, 142
  orig: 96, 142
  offset: 0, 0
  index: -1
Blue/L_hand_catgun
  rotate: false
  xy: 3279, 2408
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Blue/R_eye_catbat
  rotate: false
  xy: 1123, 2253
  size: 25, 46
  orig: 27, 48
  offset: 1, 1
  index: -1
Blue/R_hand_cataxe
  rotate: false
  xy: 1719, 437
  size: 117, 200
  orig: 117, 200
  offset: 0, 0
  index: -1
Blue/R_hand_catbat
  rotate: false
  xy: 2971, 828
  size: 106, 144
  orig: 106, 144
  offset: 0, 0
  index: -1
Blue/R_hand_catgun
  rotate: false
  xy: 2920, 1542
  size: 128, 223
  orig: 194, 223
  offset: 0, 0
  index: -1
Blue/Trigger
  rotate: false
  xy: 1495, 616
  size: 51, 21
  orig: 53, 23
  offset: 1, 1
  index: -1
Green/Trigger
  rotate: false
  xy: 1495, 616
  size: 51, 21
  orig: 53, 23
  offset: 1, 1
  index: -1
Red/Trigger
  rotate: false
  xy: 1495, 616
  size: 51, 21
  orig: 53, 23
  offset: 1, 1
  index: -1
Yellow/Trigger
  rotate: false
  xy: 1495, 616
  size: 51, 21
  orig: 53, 23
  offset: 1, 1
  index: -1
Blue/axe
  rotate: false
  xy: 647, 2046
  size: 501, 178
  orig: 503, 180
  offset: 1, 1
  index: -1
Green/axe
  rotate: false
  xy: 647, 2046
  size: 501, 178
  orig: 503, 180
  offset: 1, 1
  index: -1
Yellow/axe
  rotate: false
  xy: 647, 2046
  size: 501, 178
  orig: 503, 180
  offset: 1, 1
  index: -1
Blue/bat
  rotate: false
  xy: 1407, 1578
  size: 364, 193
  orig: 366, 195
  offset: 1, 1
  index: -1
Green/bat
  rotate: false
  xy: 1407, 1578
  size: 364, 193
  orig: 366, 195
  offset: 1, 1
  index: -1
Red/bat
  rotate: false
  xy: 1407, 1578
  size: 364, 193
  orig: 366, 195
  offset: 1, 1
  index: -1
Yellow/bat
  rotate: false
  xy: 1407, 1578
  size: 364, 193
  orig: 366, 195
  offset: 1, 1
  index: -1
Blue/body_cataxe
  rotate: false
  xy: 1845, 384
  size: 349, 265
  orig: 351, 265
  offset: 1, 0
  index: -1
Blue/body_catbat
  rotate: false
  xy: 1608, 2308
  size: 258, 197
  orig: 270, 204
  offset: 12, 0
  index: -1
Blue/body_catbat_back
  rotate: false
  xy: 3123, 2907
  size: 270, 205
  orig: 272, 207
  offset: 1, 1
  index: -1
Blue/body_catgun
  rotate: false
  xy: 1690, 2507
  size: 342, 261
  orig: 342, 283
  offset: 0, 0
  index: -1
Blue/bodypopcorn
  rotate: false
  xy: 2, 2675
  size: 482, 450
  orig: 485, 452
  offset: 2, 1
  index: -1
Blue/bodypopcorn_back
  rotate: false
  xy: 2, 6
  size: 474, 406
  orig: 476, 408
  offset: 1, 1
  index: -1
Blue/eyes_cataxe
  rotate: false
  xy: 1750, 2255
  size: 58, 51
  orig: 60, 53
  offset: 1, 1
  index: -1
Blue/eyes_catgun
  rotate: false
  xy: 2695, 100
  size: 143, 61
  orig: 145, 63
  offset: 1, 1
  index: -1
Blue/hat_catbat
  rotate: false
  xy: 2939, 562
  size: 92, 78
  orig: 94, 80
  offset: 1, 1
  index: -1
Blue/mouth_cataxe
  rotate: false
  xy: 2934, 352
  size: 81, 40
  orig: 83, 42
  offset: 1, 1
  index: -1
Blue/mouth_catbat
  rotate: false
  xy: 2381, 995
  size: 49, 32
  orig: 51, 34
  offset: 1, 1
  index: -1
Blue/nose_catgun
  rotate: false
  xy: 3063, 2137
  size: 216, 74
  orig: 219, 76
  offset: 2, 1
  index: -1
Blue/tail_cataxe
  rotate: false
  xy: 2711, 1767
  size: 263, 136
  orig: 263, 136
  offset: 0, 0
  index: -1
Blue/tail_catbat
  rotate: false
  xy: 2196, 110
  size: 238, 83
  orig: 238, 83
  offset: 0, 0
  index: -1
Blue/tail_catgun
  rotate: false
  xy: 1750, 2119
  size: 281, 134
  orig: 281, 134
  offset: 0, 0
  index: -1
Blue/whikser_catbat
  rotate: false
  xy: 2265, 2799
  size: 168, 53
  orig: 170, 56
  offset: 1, 1
  index: -1
Blue/whisker_cataxe
  rotate: false
  xy: 3079, 816
  size: 173, 78
  orig: 176, 80
  offset: 2, 1
  index: -1
Green/Gun
  rotate: false
  xy: 2196, 195
  size: 326, 224
  orig: 326, 224
  offset: 0, 0
  index: -1
Green/L_hand_cataxe
  rotate: false
  xy: 1673, 219
  size: 118, 198
  orig: 120, 198
  offset: 1, 0
  index: -1
Red/L_hand_cataxe
  rotate: false
  xy: 1673, 219
  size: 118, 198
  orig: 120, 198
  offset: 1, 0
  index: -1
Yellow/L_hand_cataxe
  rotate: false
  xy: 1673, 219
  size: 118, 198
  orig: 120, 198
  offset: 1, 0
  index: -1
Green/L_hand_catbat
  rotate: false
  xy: 3263, 676
  size: 96, 142
  orig: 96, 142
  offset: 0, 0
  index: -1
Red/L_hand_catbat
  rotate: false
  xy: 3263, 676
  size: 96, 142
  orig: 96, 142
  offset: 0, 0
  index: -1
Yellow/L_hand_catbat
  rotate: false
  xy: 3263, 676
  size: 96, 142
  orig: 96, 142
  offset: 0, 0
  index: -1
Green/L_hand_catgun
  rotate: false
  xy: 2804, 352
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Red/L_hand_catgun
  rotate: false
  xy: 2804, 352
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Yellow/L_hand_catgun
  rotate: false
  xy: 2804, 352
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Green/R_hand_cataxe
  rotate: false
  xy: 1773, 1578
  size: 117, 200
  orig: 117, 200
  offset: 0, 0
  index: -1
Red/R_hand_cataxe
  rotate: false
  xy: 1773, 1578
  size: 117, 200
  orig: 117, 200
  offset: 0, 0
  index: -1
Yellow/R_hand_cataxe
  rotate: false
  xy: 1773, 1578
  size: 117, 200
  orig: 117, 200
  offset: 0, 0
  index: -1
Green/R_hand_catbat
  rotate: false
  xy: 3201, 294
  size: 106, 144
  orig: 106, 144
  offset: 0, 0
  index: -1
Red/R_hand_catbat
  rotate: false
  xy: 3201, 294
  size: 106, 144
  orig: 106, 144
  offset: 0, 0
  index: -1
Yellow/R_hand_catbat
  rotate: false
  xy: 3201, 294
  size: 106, 144
  orig: 106, 144
  offset: 0, 0
  index: -1
Green/R_hand_catgun
  rotate: false
  xy: 3050, 1543
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Red/R_hand_catgun
  rotate: false
  xy: 3050, 1543
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Yellow/R_hand_catgun
  rotate: false
  xy: 3050, 1543
  size: 128, 223
  orig: 128, 223
  offset: 0, 0
  index: -1
Green/body_cataxe
  rotate: false
  xy: 2372, 2449
  size: 349, 265
  orig: 351, 265
  offset: 1, 0
  index: -1
Red/body_cataxe
  rotate: false
  xy: 2372, 2449
  size: 349, 265
  orig: 351, 265
  offset: 1, 0
  index: -1
Yellow/body_cataxe
  rotate: false
  xy: 2372, 2449
  size: 349, 265
  orig: 351, 265
  offset: 1, 0
  index: -1
Green/body_catbat
  rotate: false
  xy: 2711, 1905
  size: 258, 197
  orig: 270, 204
  offset: 12, 0
  index: -1
Red/body_catbat
  rotate: false
  xy: 2711, 1905
  size: 258, 197
  orig: 270, 204
  offset: 12, 0
  index: -1
Yellow/body_catbat
  rotate: false
  xy: 2711, 1905
  size: 258, 197
  orig: 270, 204
  offset: 12, 0
  index: -1
Green/body_catbat_back
  rotate: false
  xy: 2532, 370
  size: 270, 205
  orig: 272, 207
  offset: 1, 1
  index: -1
Green/body_catgun
  rotate: false
  xy: 2719, 2141
  size: 342, 261
  orig: 342, 283
  offset: 0, 0
  index: -1
Red/body_catgun
  rotate: false
  xy: 2719, 2141
  size: 342, 261
  orig: 342, 283
  offset: 0, 0
  index: -1
Yellow/body_catgun
  rotate: false
  xy: 2719, 2141
  size: 342, 261
  orig: 342, 283
  offset: 0, 0
  index: -1
Green/bodypopcorn
  rotate: false
  xy: 2, 2223
  size: 482, 450
  orig: 485, 452
  offset: 2, 1
  index: -1
Green/bodypopcorn_back
  rotate: false
  xy: 647, 2226
  size: 474, 406
  orig: 476, 408
  offset: 1, 1
  index: -1
Green/hat_catbat
  rotate: false
  xy: 1908, 94
  size: 92, 78
  orig: 94, 80
  offset: 1, 1
  index: -1
Green/tail_cataxe
  rotate: false
  xy: 3120, 2633
  size: 263, 136
  orig: 263, 136
  offset: 0, 0
  index: -1
Red/tail_cataxe
  rotate: false
  xy: 3120, 2633
  size: 263, 136
  orig: 263, 136
  offset: 0, 0
  index: -1
Yellow/tail_cataxe
  rotate: false
  xy: 3120, 2633
  size: 263, 136
  orig: 263, 136
  offset: 0, 0
  index: -1
Green/tail_catbat
  rotate: false
  xy: 1668, 39
  size: 238, 83
  orig: 238, 83
  offset: 0, 0
  index: -1
Red/tail_catbat
  rotate: false
  xy: 1668, 39
  size: 238, 83
  orig: 238, 83
  offset: 0, 0
  index: -1
Yellow/tail_catbat
  rotate: false
  xy: 1668, 39
  size: 238, 83
  orig: 238, 83
  offset: 0, 0
  index: -1
Green/tail_catgun
  rotate: false
  xy: 3123, 2771
  size: 281, 134
  orig: 281, 134
  offset: 0, 0
  index: -1
Red/tail_catgun
  rotate: false
  xy: 3123, 2771
  size: 281, 134
  orig: 281, 134
  offset: 0, 0
  index: -1
Yellow/tail_catgun
  rotate: false
  xy: 3123, 2771
  size: 281, 134
  orig: 281, 134
  offset: 0, 0
  index: -1
Happy
  rotate: false
  xy: 2984, 108
  size: 84, 78
  orig: 86, 80
  offset: 1, 1
  index: -1
L_ear
  rotate: false
  xy: 3279, 2250
  size: 113, 156
  orig: 113, 156
  offset: 0, 0
  index: -1
L_ear_blue
  rotate: false
  xy: 2856, 816
  size: 113, 156
  orig: 113, 156
  offset: 0, 0
  index: -1
R_ear
  rotate: false
  xy: 3033, 658
  size: 113, 156
  orig: 113, 156
  offset: 0, 0
  index: -1
R_ear_blue
  rotate: false
  xy: 3148, 658
  size: 113, 156
  orig: 113, 156
  offset: 0, 0
  index: -1
Red/Gun
  rotate: false
  xy: 2528, 803
  size: 326, 224
  orig: 326, 224
  offset: 0, 0
  index: -1
Red/axe
  rotate: false
  xy: 1272, 2770
  size: 503, 180
  orig: 503, 180
  offset: 0, 0
  index: -1
Red/body_catbat_back
  rotate: false
  xy: 2524, 163
  size: 270, 205
  orig: 272, 207
  offset: 1, 1
  index: -1
Yellow/body_catbat_back
  rotate: false
  xy: 2524, 163
  size: 270, 205
  orig: 272, 207
  offset: 1, 1
  index: -1
Red/bodypopcorn
  rotate: false
  xy: 2, 1771
  size: 482, 450
  orig: 485, 452
  offset: 2, 1
  index: -1
Red/bodypopcorn_back
  rotate: false
  xy: 486, 1638
  size: 474, 406
  orig: 476, 408
  offset: 1, 1
  index: -1
Red/hat_catbat
  rotate: false
  xy: 2433, 3
  size: 92, 78
  orig: 94, 80
  offset: 1, 1
  index: -1
Yellow/Gun
  rotate: false
  xy: 2532, 577
  size: 326, 224
  orig: 326, 224
  offset: 0, 0
  index: -1
Yellow/bodypopcorn
  rotate: false
  xy: 2, 1319
  size: 482, 450
  orig: 485, 452
  offset: 2, 1
  index: -1
Yellow/bodypopcorn_back
  rotate: false
  xy: 486, 1230
  size: 474, 406
  orig: 476, 408
  offset: 1, 1
  index: -1
Yellow/hat_catbat
  rotate: false
  xy: 3007, 28
  size: 92, 78
  orig: 94, 80
  offset: 1, 1
  index: -1
corn
  rotate: false
  xy: 3050, 1423
  size: 126, 118
  orig: 126, 118
  offset: 0, 0
  index: -1
effect2/changtiao
  rotate: false
  xy: 3395, 3026
  size: 21, 86
  orig: 29, 95
  offset: 4, 4
  index: -1
effect2/dian
  rotate: false
  xy: 2140, 963
  size: 15, 14
  orig: 29, 29
  offset: 7, 8
  index: -1
effect2/guang
  rotate: false
  xy: 2346, 17
  size: 85, 91
  orig: 95, 111
  offset: 7, 3
  index: -1
effect2/kuai
  rotate: false
  xy: 1971, 2059
  size: 61, 58
  orig: 76, 76
  offset: 8, 8
  index: -1
effect2/yuan01
  rotate: false
  xy: 2796, 163
  size: 186, 187
  orig: 243, 243
  offset: 30, 26
  index: -1
effect2/yuanquan
  rotate: false
  xy: 3281, 2139
  size: 109, 109
  orig: 122, 122
  offset: 7, 6
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_0
  rotate: false
  xy: 3033, 406
  size: 165, 32
  orig: 1000, 1000
  offset: 410, 85
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_10
  rotate: false
  xy: 1025, 216
  size: 646, 201
  orig: 1000, 1000
  offset: 168, 36
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_12
  rotate: false
  xy: 2465, 2896
  size: 656, 205
  orig: 1000, 1000
  offset: 159, 34
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_14
  rotate: false
  xy: 2465, 2716
  size: 653, 178
  orig: 1000, 1000
  offset: 154, 34
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_16
  rotate: false
  xy: 1025, 39
  size: 641, 175
  orig: 1000, 1000
  offset: 151, 32
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_18
  rotate: false
  xy: 440, 432
  size: 640, 156
  orig: 1000, 1000
  offset: 156, 32
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_2
  rotate: false
  xy: 478, 2
  size: 426, 134
  orig: 1000, 1000
  offset: 266, 58
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_20
  rotate: false
  xy: 3394, 2266
  size: 32, 140
  orig: 1000, 1000
  offset: 175, 50
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_4
  rotate: false
  xy: 1272, 2952
  size: 610, 182
  orig: 1000, 1000
  offset: 187, 45
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_6
  rotate: false
  xy: 645, 2947
  size: 625, 192
  orig: 1000, 1000
  offset: 181, 41
  index: -1
fx/Elements - Smoke 009 Hit Explosion Up noCT noRSZ 01/khoi_8
  rotate: false
  xy: 1082, 419
  size: 635, 195
  orig: 1000, 1000
  offset: 175, 39
  index: -1
fx/New folder/Comp 5_1
  rotate: false
  xy: 2971, 1909
  size: 196, 193
  orig: 256, 256
  offset: 30, 34
  index: -1
fx/New folder/Comp 5_2
  rotate: false
  xy: 3184, 1720
  size: 204, 205
  orig: 256, 256
  offset: 25, 26
  index: -1
fx/New folder/Comp 5_3
  rotate: false
  xy: 2708, 1558
  size: 210, 207
  orig: 256, 256
  offset: 23, 25
  index: -1
fx/New folder/Comp 5_4
  rotate: false
  xy: 1759, 1910
  size: 210, 207
  orig: 256, 256
  offset: 23, 25
  index: -1
fx/datbay/Layer 1
  rotate: false
  xy: 962, 1282
  size: 406, 377
  orig: 457, 498
  offset: 0, 20
  index: -1
fx/datbay/Layer 2
  rotate: false
  xy: 2, 843
  size: 434, 474
  orig: 457, 498
  offset: 0, 13
  index: -1
fx/datbay/Layer 3
  rotate: false
  xy: 2, 414
  size: 436, 427
  orig: 457, 498
  offset: 0, 28
  index: -1
fx/datbay/Layer 4
  rotate: false
  xy: 962, 1661
  size: 443, 383
  orig: 457, 498
  offset: 11, 19
  index: -1
fx/datbay/Layer 5
  rotate: false
  xy: 966, 937
  size: 404, 343
  orig: 457, 498
  offset: 19, 23
  index: -1
fx/nut 2
  rotate: false
  xy: 3033, 584
  size: 188, 72
  orig: 208, 116
  offset: 6, 22
  index: -1
fx/set2/set2_0
  rotate: false
  xy: 1971, 1960
  size: 43, 97
  orig: 200, 200
  offset: 70, 78
  index: -1
fx/set2/set2_12
  rotate: false
  xy: 3260, 450
  size: 82, 70
  orig: 200, 200
  offset: 54, 26
  index: -1
fx/set2/set2_14
  rotate: false
  xy: 3309, 300
  size: 75, 68
  orig: 200, 200
  offset: 57, 26
  index: -1
fx/set2/set2_2
  rotate: false
  xy: 1194, 2781
  size: 58, 164
  orig: 200, 200
  offset: 68, 9
  index: -1
fx/set2/set2_3
  rotate: false
  xy: 3254, 905
  size: 83, 163
  orig: 200, 200
  offset: 59, 6
  index: -1
fx/set2/set2_4
  rotate: false
  xy: 3297, 1204
  size: 97, 168
  orig: 200, 200
  offset: 57, 6
  index: -1
fx/set2/set2_5
  rotate: false
  xy: 2984, 188
  size: 86, 162
  orig: 200, 200
  offset: 63, 7
  index: -1
fx/set2/set2_8
  rotate: false
  xy: 2934, 394
  size: 97, 166
  orig: 200, 200
  offset: 50, 6
  index: -1
fx_water/1
  rotate: false
  xy: 3203, 2
  size: 54, 69
  orig: 514, 514
  offset: 230, 222
  index: -1
fx_water/10
  rotate: false
  xy: 1407, 1773
  size: 350, 321
  orig: 514, 514
  offset: 71, 118
  index: -1
fx_water/2
  rotate: false
  xy: 3033, 440
  size: 127, 142
  orig: 514, 514
  offset: 194, 186
  index: -1
fx_water/3
  rotate: false
  xy: 2712, 1038
  size: 171, 172
  orig: 514, 514
  offset: 172, 171
  index: -1
fx_water/4
  rotate: false
  xy: 1544, 2096
  size: 204, 203
  orig: 514, 514
  offset: 155, 155
  index: -1
fx_water/5
  rotate: false
  xy: 3070, 2423
  size: 207, 208
  orig: 514, 514
  offset: 154, 153
  index: -1
fx_water/6
  rotate: false
  xy: 2034, 2505
  size: 336, 278
  orig: 514, 514
  offset: 77, 140
  index: -1
fx_water/7
  rotate: false
  xy: 1370, 1283
  size: 331, 293
  orig: 514, 514
  offset: 76, 128
  index: -1
fx_water/8
  rotate: false
  xy: 2372, 1827
  size: 337, 308
  orig: 514, 514
  offset: 78, 123
  index: -1
fx_water/9
  rotate: false
  xy: 3079, 3114
  size: 347, 312
  orig: 514, 514
  offset: 73, 125
  index: -1
fx_water/body
  rotate: false
  xy: 906, 2
  size: 312, 35
  orig: 312, 35
  offset: 0, 0
  index: -1
fx_water/glow
  rotate: false
  xy: 2976, 1768
  size: 206, 139
  orig: 220, 146
  offset: 11, 4
  index: -1
fx_water/head
  rotate: false
  xy: 1759, 1780
  size: 192, 128
  orig: 195, 130
  offset: 2, 1
  index: -1
fx_water/particle1
  rotate: false
  xy: 2465, 3105
  size: 32, 25
  orig: 32, 25
  offset: 0, 0
  index: -1
fx_water/particle2
  rotate: false
  xy: 2432, 995
  size: 37, 32
  orig: 37, 32
  offset: 0, 0
  index: -1
fx_water/particle3
  rotate: false
  xy: 1061, 419
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
fx_water/particle4
  rotate: false
  xy: 1025, 419
  size: 16, 11
  orig: 16, 11
  offset: 0, 0
  index: -1
fx_water/particle5
  rotate: false
  xy: 3102, 3103
  size: 12, 9
  orig: 12, 9
  offset: 0, 0
  index: -1
fx_water/particle6
  rotate: false
  xy: 3079, 3103
  size: 21, 9
  orig: 21, 9
  offset: 0, 0
  index: -1
fx_water/particle7
  rotate: false
  xy: 2072, 963
  size: 66, 14
  orig: 66, 14
  offset: 0, 0
  index: -1
fx_water/water_gun1
  rotate: false
  xy: 1123, 2554
  size: 69, 78
  orig: 73, 87
  offset: 2, 3
  index: -1
fx_water/water_gun2
  rotate: false
  xy: 2695, 2
  size: 102, 96
  orig: 104, 98
  offset: 1, 1
  index: -1
fx_water/water_gun3
  rotate: false
  xy: 1777, 2867
  size: 105, 83
  orig: 107, 87
  offset: 1, 3
  index: -1
fx_water/water_gun4
  rotate: false
  xy: 3101, 41
  size: 100, 75
  orig: 102, 80
  offset: 1, 1
  index: -1
fx_water/water_gun5
  rotate: false
  xy: 2860, 577
  size: 77, 63
  orig: 84, 91
  offset: 1, 27
  index: -1
fx_water/water_gun6
  rotate: false
  xy: 3386, 1630
  size: 40, 88
  orig: 42, 90
  offset: 1, 1
  index: -1
fx_water_green/1
  rotate: false
  xy: 3259, 2
  size: 54, 69
  orig: 514, 514
  offset: 230, 222
  index: -1
fx_water_green/10
  rotate: false
  xy: 1372, 961
  size: 348, 320
  orig: 514, 514
  offset: 72, 119
  index: -1
fx_water_green/2
  rotate: false
  xy: 3072, 262
  size: 127, 142
  orig: 514, 514
  offset: 194, 186
  index: -1
fx_water_green/3
  rotate: false
  xy: 3124, 1200
  size: 171, 172
  orig: 514, 514
  offset: 172, 171
  index: -1
fx_water_green/4
  rotate: false
  xy: 3180, 1515
  size: 204, 203
  orig: 514, 514
  offset: 155, 155
  index: -1
fx_water_green/5
  rotate: false
  xy: 1793, 174
  size: 207, 208
  orig: 514, 514
  offset: 154, 153
  index: -1
fx_water_green/6
  rotate: false
  xy: 2372, 1548
  size: 334, 277
  orig: 514, 514
  offset: 78, 140
  index: -1
fx_water_green/7
  rotate: false
  xy: 1703, 1285
  size: 329, 291
  orig: 514, 514
  offset: 77, 129
  index: -1
fx_water_green/8
  rotate: false
  xy: 2034, 2197
  size: 336, 306
  orig: 514, 514
  offset: 79, 124
  index: -1
fx_water_green/9
  rotate: false
  xy: 1845, 651
  size: 345, 310
  orig: 514, 514
  offset: 74, 126
  index: -1
fx_water_green/body
  rotate: false
  xy: 1220, 2
  size: 312, 35
  orig: 312, 35
  offset: 0, 0
  index: -1
fx_water_green/glow
  rotate: false
  xy: 2712, 1417
  size: 206, 139
  orig: 220, 146
  offset: 11, 4
  index: -1
fx_water_green/head
  rotate: false
  xy: 2002, 254
  size: 192, 128
  orig: 195, 130
  offset: 2, 1
  index: -1
fx_water_green/particle1
  rotate: false
  xy: 3394, 2239
  size: 32, 25
  orig: 32, 25
  offset: 0, 0
  index: -1
fx_water_green/particle2
  rotate: false
  xy: 2471, 995
  size: 37, 32
  orig: 37, 32
  offset: 0, 0
  index: -1
fx_water_green/particle3
  rotate: false
  xy: 2019, 2494
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
fx_water_green/particle4
  rotate: false
  xy: 1848, 2854
  size: 16, 11
  orig: 16, 11
  offset: 0, 0
  index: -1
fx_water_green/particle5
  rotate: false
  xy: 1240, 2770
  size: 12, 9
  orig: 12, 9
  offset: 0, 0
  index: -1
fx_water_green/particle6
  rotate: false
  xy: 1194, 2770
  size: 21, 9
  orig: 21, 9
  offset: 0, 0
  index: -1
fx_water_green/particle7
  rotate: false
  xy: 2381, 979
  size: 66, 14
  orig: 66, 14
  offset: 0, 0
  index: -1
fx_water_green/water_gun1
  rotate: false
  xy: 1777, 2787
  size: 69, 78
  orig: 73, 87
  offset: 2, 3
  index: -1
fx_water_green/water_gun2
  rotate: false
  xy: 2799, 2
  size: 102, 96
  orig: 104, 98
  offset: 1, 1
  index: -1
fx_water_green/water_gun3
  rotate: false
  xy: 3254, 820
  size: 105, 83
  orig: 107, 87
  offset: 1, 3
  index: -1
fx_water_green/water_gun4
  rotate: false
  xy: 3203, 73
  size: 100, 75
  orig: 102, 80
  offset: 1, 1
  index: -1
fx_water_green/water_gun5
  rotate: false
  xy: 3305, 85
  size: 77, 63
  orig: 79, 65
  offset: 1, 1
  index: -1
fx_water_green/water_gun6
  rotate: false
  xy: 3386, 1540
  size: 40, 88
  orig: 42, 90
  offset: 1, 1
  index: -1
fx_water_red/1
  rotate: false
  xy: 3330, 229
  size: 54, 69
  orig: 514, 514
  offset: 230, 222
  index: -1
fx_water_red/10
  rotate: false
  xy: 1495, 639
  size: 348, 320
  orig: 514, 514
  offset: 72, 119
  index: -1
fx_water_red/2
  rotate: false
  xy: 3072, 118
  size: 127, 142
  orig: 514, 514
  offset: 194, 186
  index: -1
fx_water_red/3
  rotate: false
  xy: 3081, 896
  size: 171, 172
  orig: 514, 514
  offset: 172, 171
  index: -1
fx_water_red/4
  rotate: false
  xy: 2712, 1212
  size: 204, 203
  orig: 514, 514
  offset: 155, 155
  index: -1
fx_water_red/5
  rotate: false
  xy: 3070, 2213
  size: 207, 208
  orig: 514, 514
  offset: 154, 153
  index: -1
fx_water_red/6
  rotate: false
  xy: 2192, 700
  size: 334, 277
  orig: 514, 514
  offset: 78, 140
  index: -1
fx_water_red/7
  rotate: false
  xy: 2034, 1288
  size: 329, 291
  orig: 514, 514
  offset: 77, 129
  index: -1
fx_water_red/8
  rotate: false
  xy: 2034, 1889
  size: 336, 306
  orig: 514, 514
  offset: 79, 124
  index: -1
fx_water_red/9
  rotate: false
  xy: 2723, 2404
  size: 345, 310
  orig: 514, 514
  offset: 74, 126
  index: -1
fx_water_red/body
  rotate: false
  xy: 1534, 2
  size: 312, 35
  orig: 312, 35
  offset: 0, 0
  index: -1
fx_water_red/glow
  rotate: false
  xy: 2885, 1070
  size: 206, 139
  orig: 220, 146
  offset: 11, 4
  index: -1
fx_water_red/head
  rotate: false
  xy: 2002, 124
  size: 192, 128
  orig: 195, 130
  offset: 2, 1
  index: -1
fx_water_red/particle1
  rotate: false
  xy: 906, 48
  size: 32, 25
  orig: 32, 25
  offset: 0, 0
  index: -1
fx_water_red/particle2
  rotate: false
  xy: 3033, 372
  size: 37, 32
  orig: 37, 32
  offset: 0, 0
  index: -1
fx_water_red/particle3
  rotate: false
  xy: 3169, 1914
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
fx_water_red/particle4
  rotate: false
  xy: 1866, 2854
  size: 16, 11
  orig: 16, 11
  offset: 0, 0
  index: -1
fx_water_red/particle5
  rotate: false
  xy: 633, 2623
  size: 12, 9
  orig: 12, 9
  offset: 0, 0
  index: -1
fx_water_red/particle6
  rotate: false
  xy: 3093, 1200
  size: 21, 9
  orig: 21, 9
  offset: 0, 0
  index: -1
fx_water_red/particle7
  rotate: false
  xy: 2449, 979
  size: 66, 14
  orig: 66, 14
  offset: 0, 0
  index: -1
fx_water_red/water_gun1
  rotate: false
  xy: 2372, 2719
  size: 69, 78
  orig: 73, 87
  offset: 2, 3
  index: -1
fx_water_red/water_gun2
  rotate: false
  xy: 2903, 10
  size: 102, 96
  orig: 104, 98
  offset: 1, 1
  index: -1
fx_water_red/water_gun3
  rotate: false
  xy: 2239, 25
  size: 105, 83
  orig: 107, 87
  offset: 1, 3
  index: -1
fx_water_red/water_gun4
  rotate: false
  xy: 3263, 599
  size: 100, 75
  orig: 102, 80
  offset: 1, 1
  index: -1
fx_water_red/water_gun5
  rotate: false
  xy: 3315, 20
  size: 77, 63
  orig: 79, 65
  offset: 1, 1
  index: -1
fx_water_red/water_gun6
  rotate: false
  xy: 3386, 1450
  size: 40, 88
  orig: 42, 90
  offset: 1, 1
  index: -1
fx_water_yellow/1
  rotate: false
  xy: 3330, 158
  size: 54, 69
  orig: 514, 514
  offset: 230, 222
  index: -1
fx_water_yellow/10
  rotate: false
  xy: 1722, 963
  size: 348, 320
  orig: 514, 514
  offset: 72, 119
  index: -1
fx_water_yellow/2
  rotate: false
  xy: 3201, 150
  size: 127, 142
  orig: 514, 514
  offset: 194, 186
  index: -1
fx_water_yellow/3
  rotate: false
  xy: 2860, 642
  size: 171, 172
  orig: 514, 514
  offset: 172, 171
  index: -1
fx_water_yellow/4
  rotate: false
  xy: 2918, 1211
  size: 204, 203
  orig: 514, 514
  offset: 155, 155
  index: -1
fx_water_yellow/5
  rotate: false
  xy: 3169, 1927
  size: 207, 208
  orig: 514, 514
  offset: 154, 153
  index: -1
fx_water_yellow/6
  rotate: false
  xy: 2196, 421
  size: 334, 277
  orig: 514, 514
  offset: 78, 140
  index: -1
fx_water_yellow/7
  rotate: false
  xy: 2381, 1029
  size: 329, 291
  orig: 514, 514
  offset: 77, 129
  index: -1
fx_water_yellow/8
  rotate: false
  xy: 2034, 1581
  size: 336, 306
  orig: 514, 514
  offset: 79, 124
  index: -1
fx_water_yellow/9
  rotate: false
  xy: 2372, 2137
  size: 345, 310
  orig: 514, 514
  offset: 74, 126
  index: -1
fx_water_yellow/body
  rotate: false
  xy: 2719, 2104
  size: 312, 35
  orig: 312, 35
  offset: 0, 0
  index: -1
fx_water_yellow/glow
  rotate: false
  xy: 3178, 1374
  size: 206, 139
  orig: 220, 146
  offset: 11, 4
  index: -1
fx_water_yellow/head
  rotate: false
  xy: 3093, 1070
  size: 192, 128
  orig: 195, 130
  offset: 2, 1
  index: -1
fx_water_yellow/particle1
  rotate: false
  xy: 1243, 2069
  size: 32, 25
  orig: 32, 25
  offset: 0, 0
  index: -1
fx_water_yellow/particle2
  rotate: false
  xy: 3070, 2682
  size: 37, 32
  orig: 37, 32
  offset: 0, 0
  index: -1
fx_water_yellow/particle3
  rotate: false
  xy: 2019, 2481
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
fx_water_yellow/particle4
  rotate: false
  xy: 1043, 419
  size: 16, 11
  orig: 16, 11
  offset: 0, 0
  index: -1
fx_water_yellow/particle5
  rotate: false
  xy: 633, 2612
  size: 12, 9
  orig: 12, 9
  offset: 0, 0
  index: -1
fx_water_yellow/particle6
  rotate: false
  xy: 1217, 2770
  size: 21, 9
  orig: 21, 9
  offset: 0, 0
  index: -1
fx_water_yellow/particle7
  rotate: false
  xy: 486, 2047
  size: 66, 14
  orig: 66, 14
  offset: 0, 0
  index: -1
fx_water_yellow/water_gun1
  rotate: false
  xy: 3309, 370
  size: 69, 78
  orig: 73, 87
  offset: 2, 3
  index: -1
fx_water_yellow/water_gun2
  rotate: false
  xy: 2135, 12
  size: 102, 96
  orig: 104, 98
  offset: 1, 1
  index: -1
fx_water_yellow/water_gun3
  rotate: false
  xy: 2527, 7
  size: 105, 83
  orig: 107, 87
  offset: 1, 3
  index: -1
fx_water_yellow/water_gun4
  rotate: false
  xy: 3260, 522
  size: 100, 75
  orig: 102, 80
  offset: 1, 1
  index: -1
fx_water_yellow/water_gun5
  rotate: false
  xy: 1953, 1845
  size: 77, 63
  orig: 79, 65
  offset: 1, 1
  index: -1
fx_water_yellow/water_gun6
  rotate: false
  xy: 2634, 2
  size: 40, 88
  orig: 42, 90
  offset: 1, 1
  index: -1
hit_dead
  rotate: false
  xy: 2920, 1416
  size: 128, 124
  orig: 131, 127
  offset: 2, 2
  index: -1
shadow
  rotate: false
  xy: 1848, 2785
  size: 415, 67
  orig: 415, 67
  offset: 0, 0
  index: -1
star
  rotate: false
  xy: 2019, 8
  size: 114, 114
  orig: 122, 122
  offset: 4, 4
  index: -1
star_circle
  rotate: false
  xy: 2072, 979
  size: 307, 307
  orig: 307, 307
  offset: 0, 0
  index: -1
t_0002
  rotate: false
  xy: 1884, 2854
  size: 579, 276
  orig: 896, 896
  offset: 257, 301
  index: -1
t_0003
  rotate: false
  xy: 1898, 3132
  size: 599, 294
  orig: 896, 896
  offset: 248, 304
  index: -1
t_0004
  rotate: false
  xy: 1279, 3136
  size: 617, 290
  orig: 896, 896
  offset: 238, 301
  index: -1
t_0005
  rotate: false
  xy: 645, 3141
  size: 632, 285
  orig: 896, 896
  offset: 232, 297
  index: -1
t_0006
  rotate: false
  xy: 2, 3127
  size: 641, 299
  orig: 896, 896
  offset: 226, 296
  index: -1
t_0007
  rotate: false
  xy: 478, 138
  size: 545, 292
  orig: 896, 896
  offset: 262, 318
  index: -1
t_0008
  rotate: false
  xy: 440, 590
  size: 541, 307
  orig: 896, 896
  offset: 276, 315
  index: -1
t_0009
  rotate: false
  xy: 633, 2634
  size: 559, 311
  orig: 896, 896
  offset: 268, 311
  index: -1
t_0010
  rotate: false
  xy: 2499, 3103
  size: 578, 323
  orig: 896, 896
  offset: 257, 309
  index: -1
t_0011
  rotate: false
  xy: 438, 899
  size: 526, 329
  orig: 896, 896
  offset: 262, 312
  index: -1
t_0012
  rotate: false
  xy: 983, 616
  size: 510, 319
  orig: 896, 896
  offset: 267, 317
  index: -1
t_0013
  rotate: false
  xy: 1194, 2509
  size: 494, 259
  orig: 896, 896
  offset: 273, 324
  index: -1
t_0014
  rotate: false
  xy: 1123, 2301
  size: 483, 206
  orig: 896, 896
  offset: 276, 372
  index: -1
t_0015
  rotate: false
  xy: 1150, 2096
  size: 392, 203
  orig: 896, 896
  offset: 367, 370
  index: -1
t_0016
  rotate: false
  xy: 2885, 974
  size: 194, 94
  orig: 896, 896
  offset: 545, 372
  index: -1
t_0017
  rotate: false
  xy: 2524, 92
  size: 169, 69
  orig: 896, 896
  offset: 549, 377
  index: -1
t_0018
  rotate: false
  xy: 1150, 2046
  size: 91, 48
  orig: 896, 896
  offset: 624, 395
  index: -1
tt_0005
  rotate: false
  xy: 1868, 2255
  size: 149, 250
  orig: 896, 896
  offset: 378, 320
  index: -1
tt_0006
  rotate: false
  xy: 486, 2063
  size: 159, 514
  orig: 896, 896
  offset: 373, 188
  index: -1
tt_0007
  rotate: false
  xy: 486, 2579
  size: 145, 546
  orig: 896, 896
  offset: 394, 175
  index: -1
tt_0008
  rotate: false
  xy: 438, 1251
  size: 45, 66
  orig: 896, 896
  offset: 414, 299
  index: -1
